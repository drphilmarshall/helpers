# Welcome to Yao-Yuan Mao's helpers!

This repository contains a set of useful, but not necessarily related, Python scripts that carry out or accelerate many different tasks in my research. Most of them involve dark matter simulations. In particular, you can find Pyhton scripts that

- load the Gadget-2 snapshots,
- load the ascii outputs from Peter Behroozi's Rockstar and Consistent Trees,
- caluclate projected correlation functions with error estimation from jackknife,

and many more other things!

## Installation

    sudo python setup.py install
    
Then, in python, try (for example):

    import helpers
    from helpers.SimulationAnalysis import readHlist

