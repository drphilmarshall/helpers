#!/usr/bin/env python

from distutils.core import setup

setup(name='helpers',
      version='0.1',
      description='Cosmological simulation analysis utilities',
      author='Yao-Yuan Mao',
      author_email='maomao.321@gmail.com',
      url='https://bitbucket.org/yymao/helpers/src', 
      packages = ['helpers'],
      py_modules = ['CorrelationFunction','getMUSICregion','SimulationAnalysis','getSDSSid','mvee','distributeMUSICBndryPart','readGadgetSnapshot','findLagrangianVolume','findSpheresInSnapshots']
     )
